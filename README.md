# To run the application:
    - Java 8 needed to run the program
    - It has a gradle wrapper so not needed to have any kind of gradle installed.

    - ./gradlew clean build
    - java -jar build/libs/meltwatersmsc-0.0.1-SNAPSHOT.jar --path=<path to file>
    - The input path file should contain lines that allowed in the program
    - Each line should have an end line at the end.
    - Test files are under the example folder to test the app

# How could it be better.
    - First of all sorry that I could not write more tests. Unfortunately I had no time for that but in person I
    can write as much as needed.
    - Improve code coverage
    - Improve the line processing methodology (not using splitting)
    - Rething the interface pattern. Maybe using abstract class that calls default methods.
