package com.meltwater.smsc.common.domain;

import com.meltwater.smsc.common.domain.Command;

public interface CommandExecutor {
    boolean canExecute(Command command);
    void execute(Command command);
}
