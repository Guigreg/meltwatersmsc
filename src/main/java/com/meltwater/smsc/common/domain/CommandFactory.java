package com.meltwater.smsc.common.domain;

import com.meltwater.smsc.common.domain.Command;

public interface CommandFactory {
    boolean canProcess(String line);
    Command process(String line);
}
