package com.meltwater.smsc.common.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table
@Data
public class Phone implements Serializable {
    @Id
    private String name;
    private String number;
    private boolean subscribed;
}
