package com.meltwater.smsc.common.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;

@Table
@Entity
@Data
public class PhoneGroup {
    @Id
    private String groupName;

    @Column(length = 100000)
    private ArrayList<Phone> phoneNumbers = new ArrayList<>();
}
