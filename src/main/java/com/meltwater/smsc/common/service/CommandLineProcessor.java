package com.meltwater.smsc.common.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.SimpleCommandLinePropertySource;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CommandLineProcessor implements CommandLineRunner {
    private static final String PATH = "path";
    private final FileReaderService fileReaderService;

    @Override
    public void run(String... args) throws Exception {
        SimpleCommandLinePropertySource ps = new SimpleCommandLinePropertySource(args);
        String path = ps.getProperty(PATH);
        if (path == null) {
            throw new IllegalArgumentException("Command line parameter missing. Give it like \"--path=/path/to/file\"");
        }
        log.debug("The PATH what you gave: {}", path);

        fileReaderService.readAndProcess(path);
    }
}
