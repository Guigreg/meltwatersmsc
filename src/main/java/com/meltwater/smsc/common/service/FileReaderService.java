package com.meltwater.smsc.common.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileReaderService {
    private final LineValidatorService lineValidatorService;
    private final LineProcessor lineProcessor;

    public void readAndProcess(String path) throws IOException {
        Path filePath = Paths.get(path);
        Scanner scanner = new Scanner(filePath);
        String line;
        while (scanner.hasNext()) {
            line = scanner.nextLine();
            log.debug("Validating and processing line {}", line);
            lineValidatorService.validateLine(line);
            lineProcessor.processLine(line);
        }
    }
}
