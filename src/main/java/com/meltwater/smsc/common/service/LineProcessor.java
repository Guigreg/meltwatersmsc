package com.meltwater.smsc.common.service;

import com.meltwater.smsc.common.domain.Command;
import com.meltwater.smsc.common.domain.CommandExecutor;
import com.meltwater.smsc.common.domain.CommandFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class LineProcessor {
    private final Set<CommandExecutor> commandExecutors;
    private final Set<CommandFactory> commandFactories;

    public void processLine(String line) {
        commandFactories.forEach(commandFactory -> {
            if (commandFactory.canProcess(line)) {
                Command command = commandFactory.process(line);
                commandExecutors.forEach(commandExecutor -> {
                    if(commandExecutor.canExecute(command)) {
                        commandExecutor.execute(command);
                    }
                });
            }
        });
    }
}
