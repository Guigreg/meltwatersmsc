package com.meltwater.smsc.common.service;

import com.meltwater.smsc.common.domain.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

@Slf4j
@Service
public class LineValidatorService {
    private static final Predicate<String> NUMBER_COMMAND_REGEX = Pattern.compile("(number\\d+)\\s(.+)").asPredicate();
    private static final Predicate<String> SUBSCRIBE_COMMAND_REGEX = Pattern.compile("subscribe\\s(.+)").asPredicate();
    private static final Predicate<String> UNSUBSCRIBE_COMMAND_REGEX = Pattern.compile("unsubscribe\\s(.+)").asPredicate();
    private static final Predicate<String> GROUP_COMMAND_REGEX = Pattern.compile("(group\\d+)(\\s\\S+)+").asPredicate();
    private static final Predicate<String> MESSAGE_COMMAND_REGEX = Pattern.compile("message\\s(\\w+\\d+,?)\\s(\\w+\\d+,?)+\\s(\".*\")").asPredicate();
    private final Set<Predicate<String>> validator = new HashSet<>(Arrays.asList(
            NUMBER_COMMAND_REGEX,
            SUBSCRIBE_COMMAND_REGEX,
            UNSUBSCRIBE_COMMAND_REGEX,
            GROUP_COMMAND_REGEX,
            MESSAGE_COMMAND_REGEX
    ));

    public void validateLine(String line) {
        long count = validator.stream().filter(v -> v.test(line)).count();
        if (count == 0) {
            throw new ValidationException(String.format("%s line not matching for any validation.", line));
        }
    }
}
