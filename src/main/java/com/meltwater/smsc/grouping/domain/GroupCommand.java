package com.meltwater.smsc.grouping.domain;

import com.meltwater.smsc.common.domain.Command;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class GroupCommand implements Command {
    private String groupName;
    private List<String> patterns;
}
