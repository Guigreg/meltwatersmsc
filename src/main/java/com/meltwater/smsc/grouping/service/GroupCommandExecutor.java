package com.meltwater.smsc.grouping.service;

import com.meltwater.smsc.common.domain.Command;
import com.meltwater.smsc.grouping.domain.GroupCommand;
import com.meltwater.smsc.common.domain.Phone;
import com.meltwater.smsc.common.domain.PhoneGroup;
import com.meltwater.smsc.repository.GroupRepository;
import com.meltwater.smsc.repository.PhoneRepository;
import com.meltwater.smsc.common.domain.CommandExecutor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class GroupCommandExecutor implements CommandExecutor {
    private final GroupRepository groupRepository;
    private final PhoneRepository phoneRepository;

    @Override
    public boolean canExecute(Command command) {
        return command instanceof GroupCommand;
    }

    @Override
    public void execute(Command command) {
        if (!canExecute(command))
            throw new UnsupportedOperationException("This command can be executed");

        GroupCommand groupCommand = (GroupCommand) command;
        List<Phone> phonesLikePattern = getPhones(groupCommand.getPatterns());
        setPhonesGroup(phonesLikePattern, groupCommand.getGroupName());
    }

    private void setPhonesGroup(List<Phone> listOfPhones, String groupName) {
        PhoneGroup phoneGroup = new PhoneGroup();
        phoneGroup.setGroupName(groupName);
        phoneGroup.setPhoneNumbers(listOfPhones.stream().collect(Collectors.toCollection(ArrayList::new)));
        groupRepository.save(phoneGroup);
    }

    private List<Phone> getPhones(List<String> patterns) {
        List<Phone> output = new ArrayList<>();
        patterns.forEach(s -> getPhoneLikePattern(s).forEach(output::add));
        return output;
    }

    private List<Phone> getPhoneLikePattern(String pattern) {
        if (pattern.contains("*")) {
            pattern = pattern.replace("*", "%");
        }
        List<Phone> listOfPhones = phoneRepository.findLike(pattern);
        if (listOfPhones == null || listOfPhones.isEmpty()) {
            log.error("No phones found for pattern: {}. Grouping will have no result", pattern);
        }
        log.debug("Phones for {} pattern found {}", pattern, listOfPhones);
        return listOfPhones;
    }
}
