package com.meltwater.smsc.grouping.service;

import com.meltwater.smsc.common.domain.Command;
import com.meltwater.smsc.grouping.domain.GroupCommand;
import com.meltwater.smsc.common.domain.CommandFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class GroupCommandFactory implements CommandFactory {
    private static final String GROUP_PREFIX = "group";
    private static final String WHITESPACE = " ";

    @Override
    public boolean canProcess(String line) {
        return line.startsWith(GROUP_PREFIX);
    }

    @Override
    public Command process(String line) {
        if(!canProcess(line))
            throw new UnsupportedOperationException("This line cannot be paresed with this factory");

        String[] s = line.split(WHITESPACE);

        return GroupCommand.builder()
                .groupName(getCommand(s))
                .patterns(getPatterns(s))
                .build();
    }

    private String getCommand(String[] s) {
        return s[0];
    }

    private List<String> getPatterns(String[] s) {
        return Arrays.asList(Arrays.copyOfRange(s, 1, s.length));
    }
}
