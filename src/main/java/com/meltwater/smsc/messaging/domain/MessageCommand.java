package com.meltwater.smsc.messaging.domain;

import com.meltwater.smsc.common.domain.Command;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class MessageCommand implements Command {
    private String from;
    private List<String> to;
    private String message;
}
