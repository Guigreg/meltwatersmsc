package com.meltwater.smsc.messaging.domain;

import com.meltwater.smsc.common.domain.Phone;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class MessageCommandResolved {
    private Phone from;
    private List<Phone> to;
    private String message;
}
