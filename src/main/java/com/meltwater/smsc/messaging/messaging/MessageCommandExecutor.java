package com.meltwater.smsc.messaging.messaging;

import com.meltwater.smsc.common.domain.*;
import com.meltwater.smsc.messaging.domain.MessageCommand;
import com.meltwater.smsc.messaging.domain.MessageCommandResolved;
import com.meltwater.smsc.repository.GroupRepository;
import com.meltwater.smsc.repository.PhoneRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageCommandExecutor implements CommandExecutor {
    private final PhoneRepository phoneRepository;
    private final GroupRepository groupRepository;

    @Override
    public boolean canExecute(Command command) {
        return command instanceof MessageCommand;
    }

    @Override
    public void execute(Command command) {
        if (!canExecute(command))
            throw new UnsupportedOperationException("This command can be executed");

        MessageCommand messageCommand = (MessageCommand) command;
        MessageCommandResolved resolvedCommand;
        try {
            resolvedCommand = resolve(messageCommand);
        } catch (EntityNotFoundException e) {
            log.error("Error happened during resolving: {}", e.getMessage());
            return;
        }

        log.info("MESSAGE SENT: {} {} {}", resolvedCommand.getFrom().getNumber(), createToString(resolvedCommand.getTo()),
                resolvedCommand.getMessage());
    }

    private String createToString(List<Phone> list) {
        StringBuilder stringBuilder = new StringBuilder();
        list.forEach(phone -> stringBuilder.append(phone.getNumber()).append(" "));
        return stringBuilder.toString();
    }

    private MessageCommandResolved resolve(MessageCommand command) {
        Phone from = getPhone(command.getFrom());

        List<Phone> toList = new ArrayList<>();
        command.getTo().forEach(name -> toList.addAll(getPhones(name)));

        for (Phone phone : toList) {
            if (!phone.isSubscribed())
                throw new EntityNotFoundException(String.format(
                        "Message cannot be sent! Phone %s wasn't subscribed at the moment!", phone.getName()));
        }

        return MessageCommandResolved.builder()
                .from(from)
                .to(toList)
                .message(command.getMessage())
                .build();
    }

    private List<Phone> getPhones(String name) {
        if (name.startsWith("group")) {
            return getPhonesInGroup(name);
        } else if (name.startsWith("broadcast")) {
            return phoneRepository.findAllSubscribed();
        }
        return Arrays.asList(getPhone(name));
    }

    private List<Phone> getPhonesInGroup(String groupName) {
        PhoneGroup phoneGroup = groupRepository.findOne(groupName);
        if (phoneGroup == null || phoneGroup.getPhoneNumbers().isEmpty()) {
            throw new EntityNotFoundException(String.format("Recipient phoneGroup with %s name cannot be resolved", groupName));
        }
        return phoneGroup.getPhoneNumbers();
    }

    private Phone getPhone(String name) {
        Phone phone = phoneRepository.findOne(name);
        if (phone == null) {
            throw new EntityNotFoundException(String.format("Sender or recipient with name: %s is not " +
                    "found in system!", name));
        }
        return phone;
    }
}
