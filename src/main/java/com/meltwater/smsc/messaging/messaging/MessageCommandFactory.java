package com.meltwater.smsc.messaging.messaging;

import com.meltwater.smsc.common.domain.Command;
import com.meltwater.smsc.common.domain.CommandFactory;
import com.meltwater.smsc.messaging.domain.MessageCommand;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MessageCommandFactory implements CommandFactory {
    private static final String MESSAGE_PREFIX = "message";
    private static final String COMMA = ",";
    private static final String WHITESPACE = " ";
    private static final int LIMIT = 4;

    @Override
    public boolean canProcess(String line) {
        return line.startsWith(MESSAGE_PREFIX);
    }

    @Override
    public Command process(String line) {
        if(!canProcess(line))
            throw new UnsupportedOperationException("This line cannot be paresed with this factory");
            
        String[] s = line.split(WHITESPACE, LIMIT);

        return MessageCommand.builder()
                .from(getFrom(s))
                .to(getTo(s))
                .message(getMessage(s))
                .build();
    }

    private String getFrom(String[] splittedString) {
        return splittedString[1];
    }

    private List<String> getTo(String[] splittedString) {
        String to = splittedString[2];
        List<String> toList = new ArrayList<>();
        if (to.contains(COMMA)) {
            String[] splittedByComma = to.split(COMMA);
            Arrays.asList(splittedByComma).forEach(toList::add);
            return toList;
        }
        toList.add(to);
        return toList;
    }

    private String getMessage(String[] splittedString) {
        return splittedString[3];
    }
}