package com.meltwater.smsc.phonehandling.domain;

import com.meltwater.smsc.common.domain.Command;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PhoneHandlingCommand implements Command {
    private PhoneHandlingEnum command;
    private String key;
    private String value;
}
