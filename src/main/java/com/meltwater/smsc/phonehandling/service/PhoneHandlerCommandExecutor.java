package com.meltwater.smsc.phonehandling.service;

import com.meltwater.smsc.common.domain.Command;
import com.meltwater.smsc.common.domain.Phone;
import com.meltwater.smsc.phonehandling.domain.PhoneHandlingCommand;
import com.meltwater.smsc.phonehandling.domain.PhoneHandlingEnum;
import com.meltwater.smsc.repository.PhoneRepository;
import com.meltwater.smsc.common.domain.CommandExecutor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PhoneHandlerCommandExecutor implements CommandExecutor {
    private final PhoneRepository phoneRepository;

    @Override
    public boolean canExecute(Command command) {
        return command instanceof PhoneHandlingCommand;
    }

    @Override
    public void execute(Command command) {
        if (!canExecute(command))
            throw new UnsupportedOperationException("This command can be executed");

        PhoneHandlingCommand phoneHandlingCommand = (PhoneHandlingCommand) command;
        if (PhoneHandlingEnum.ADD.equals(phoneHandlingCommand.getCommand())) {
            savePhone(phoneHandlingCommand);
            return;
        } else if (PhoneHandlingEnum.SUBSCRIBE.equals(phoneHandlingCommand.getCommand())) {
            subscribe(phoneHandlingCommand.getValue());
            return;
        } else if (PhoneHandlingEnum.UNSUBSCRIBE.equals(phoneHandlingCommand.getCommand())) {
            unsubscribe(phoneHandlingCommand.getValue());
            return;
        }
        throw new UnsupportedOperationException();
    }

    private void savePhone(PhoneHandlingCommand command) {
        Phone phone = new Phone();
        phone.setName(command.getKey());
        phone.setNumber(command.getValue());

        Phone saved = phoneRepository.save(phone);
        log.debug("Phone saved: {}", saved);
    }

    public void subscribe(String name) {
        log.debug("Findig phone by name: {}", name);
        Phone entity = phoneRepository.findOne(name);

        if (entity == null) {
            throw new RuntimeException("Phone number not found!");
        }
        entity.setSubscribed(true);
        Phone returnedPhone = phoneRepository.save(entity);
        log.debug("Phone after subscribed {}", returnedPhone);
    }

    public void unsubscribe(String name) {
        log.debug("Findig phone by name: {}", name);
        Phone entity = phoneRepository.findOne(name);
        if (entity == null) {
            throw new RuntimeException("Phone number not found!");
        }
        entity.setSubscribed(false);
        Phone returnedPhone = phoneRepository.save(entity);
        log.debug("Phone after unsubscribed: {}", returnedPhone);
    }
}
