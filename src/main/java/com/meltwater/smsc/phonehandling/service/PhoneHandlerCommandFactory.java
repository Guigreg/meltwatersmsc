package com.meltwater.smsc.phonehandling.service;

import com.meltwater.smsc.common.domain.Command;
import com.meltwater.smsc.phonehandling.domain.PhoneHandlingCommand;
import com.meltwater.smsc.phonehandling.domain.PhoneHandlingEnum;
import com.meltwater.smsc.common.domain.CommandFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PhoneHandlerCommandFactory implements CommandFactory {
    private static final String WHITESPACE = " ";
    private static final String SUBSCRIBE_POSTFIX = "subscribe";
    private static final String UNSUBSCRIBE_POSTFIX = "unsubscribe";
    private static final String NUMBER_POSTFIX = "number";

    @Override
    public boolean canProcess(String line) {
        return line.startsWith(SUBSCRIBE_POSTFIX) || line.startsWith(UNSUBSCRIBE_POSTFIX) ||
                line.startsWith(NUMBER_POSTFIX);
    }

    @Override
    public Command process(String line) {
        if(!canProcess(line))
            throw new UnsupportedOperationException("This line cannot be paresed with this factory");

        String[] s = line.split(WHITESPACE);
        if (line.startsWith(SUBSCRIBE_POSTFIX)) {
            return PhoneHandlingCommand.builder()
                    .command(PhoneHandlingEnum.SUBSCRIBE)
                    .value(getValue(s))
                    .build();

        } else if (line.startsWith(UNSUBSCRIBE_POSTFIX)) {
            return PhoneHandlingCommand.builder()
                    .command(PhoneHandlingEnum.UNSUBSCRIBE)
                    .value(getValue(s))
                    .build();
        } else if (line.startsWith(NUMBER_POSTFIX)) {
            return PhoneHandlingCommand.builder()
                    .command(PhoneHandlingEnum.ADD)
                    .key(getKey(s))
                    .value(getValue(s))
                    .build();
        }
        throw new UnsupportedOperationException();
    }

    private String getValue(String[] splittedCommand) {
        return splittedCommand[1];
    }

    private String getKey(String[] splittedCommand) {
        return splittedCommand[0];
    }
}
