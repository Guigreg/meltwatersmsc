package com.meltwater.smsc.repository;

import com.meltwater.smsc.common.domain.PhoneGroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends CrudRepository<PhoneGroup, String> {
    @Override
    PhoneGroup findOne(String groupName);

    @Override
    PhoneGroup save(PhoneGroup entity);
}
