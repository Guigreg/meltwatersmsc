package com.meltwater.smsc.repository;

import com.meltwater.smsc.common.domain.Phone;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface PhoneRepository extends CrudRepository<Phone, String> {
    @Override
    Phone findOne(String name);

    @Query("SELECT p FROM Phone p WHERE subscribed = true")
    ArrayList<Phone> findAllSubscribed();

    @Override
    Phone save(Phone entity);

    @Query("SELECT p FROM Phone p WHERE number LIKE :pattern")
    ArrayList<Phone> findLike(@Param("pattern") String pattern);
}
