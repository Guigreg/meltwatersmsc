package com.meltwater.smsc.grouping.service;

import com.meltwater.smsc.grouping.domain.GroupCommand;
import com.meltwater.smsc.messaging.domain.MessageCommand;
import com.meltwater.smsc.repository.GroupRepository;
import com.meltwater.smsc.repository.PhoneRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class GroupCommandExecutorTest {
    private static final String GROUP_NAME = "groupName";

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private PhoneRepository phoneRepository;

    @InjectMocks
    private GroupCommandExecutor underTest;

    @Test
    public void canExecute_withExpectedParam_shouldReturnTrue() throws Exception {
        GroupCommand command = GroupCommand.builder().groupName(GROUP_NAME).build();

        boolean actual = underTest.canExecute(command);

        Assert.assertTrue(actual);
    }

    @Test
    public void canExecute_withWrongParam_shouldRetrunFalse() throws Exception {
        MessageCommand command = MessageCommand.builder().build();

        boolean actual = underTest.canExecute(command);

        Assert.assertFalse(actual);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void execute_withNotExpectedParam_shouldThrowException() throws Exception {
        MessageCommand command = MessageCommand.builder().build();

        underTest.execute(command);
    }

}